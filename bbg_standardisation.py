import numpy as np
import pandas as pd
import re

def get_voting_rights(filepath, validate=True):
    ''' 
    Return BBG voting rights and other relevant details of each target company's 
    outstanding securities.
    Parameters:
        filepath         : String path that contains all the securities' details
        validate         : Boolean flag to indicate whether user wishes to validate
                           the results first. Default value is True.
    Return:
        df_securities    : DataFrame containing the security details of all target companies
    ''' 
    # Initialise parameter #
    securities = np.empty((0,7), str)
    
    # Parse text file #
    with open(filepath) as file:
        lines = file.readlines()
    file.close()
    
    # Extract relevant information #
    for line in lines:
        ## Search for sciBetaId ##
        if re.search('{', line):
            # sciBetaid = re.sub(r'{([A-Z0-9]+)}', r'\1', line).strip()
            sciBetaid = re.search(r'\w+[-]\d+', line).group(0)
        ## Search for company name ##
        if re.search('Currency', line):
            company_name = line.split('Currency')[0].strip().upper()
            company_name = company_name.split('EXCLUDE OPERATING LEASES')[0].strip()
        ## Search for security details ##
        if re.search(r'\d{2}\)', line):
            chunks = re.split('\t', line)[1:]
            if len(chunks) == 8:
                securities = np.vstack((securities, 
                                        np.array([[sciBetaid, company_name, 
                                                   chunks[0], chunks[1], chunks[2], 
                                                   chunks[4], chunks[-2]]])))
            else:
                print(sciBetaid, company_name, chunks)
    
    # Convert array into DataFrame #
    df_securities = pd.DataFrame(securities, 
                                 columns=['SCIBETA_ID', 'COMPANY_BBG', 
                                          'TICKER', 'COUNTRY', 
                                          'SECURITY_CLASS', 'SHARES_OUTSTANDING',
                                          'VOTING_RIGHT'])
    
    # Standardise the data #
    df_securities['SHARES_OUTSTANDING'] = df_securities['SHARES_OUTSTANDING'].str.replace(',', '')
    df_securities.loc[df_securities['SHARES_OUTSTANDING'] == ' ', 'SHARES_OUTSTANDING'] = np.nan
    df_securities['SHARES_OUTSTANDING'] = df_securities['SHARES_OUTSTANDING'].astype(float)
    df_securities['VOTING_RIGHT'] = df_securities['VOTING_RIGHT'].str.replace(',', '')
    df_securities.loc[df_securities['VOTING_RIGHT'] == ' ', 'VOTING_RIGHT'] = np.nan
    df_securities['VOTING_RIGHT'] = df_securities['VOTING_RIGHT'].astype(float).fillna(-1.0)
    
    # Import original company names for manual validation #
    if validate:
        new_path = '\\'.join(filepath.split('\\')[:-1]) + r'\SBSU_BBG.xlsx'
        df_temp = pd.read_excel(new_path, usecols=['sciBetaId', 'COMPANY'])
        df_securities = pd.merge(df_securities, df_temp, how='left',
                                 left_on='SCIBETA_ID', right_on='sciBetaId')
        df_securities = df_securities[['SCIBETA_ID', 'COMPANY_BBG', 'COMPANY',
                                       'TICKER', 'COUNTRY', 'SECURITY_CLASS', 
                                       'SHARES_OUTSTANDING', 'VOTING_RIGHT']]
    
    # Download Excel file #
    new_path = '\\'.join(filepath.split('\\')[:-1]) + r'\BBG_VOTING_RIGHTS.xlsx'
    df_securities.to_excel(new_path, index=False)
    
    return df_securities