# Overview

This project documents the quarterly Bloomberg (BBG) voting rights extraction and pre-processing processes. The Uipath script is used for extraction, while the Python scripts helps in the data standardisation.

*Please clone this repository in local machine before commencement.*

---

## Extraction

We have developed a UiPath script to query a target company via BBG interface and extract the company's voting rights (of all outstanding listed and unlisted securities).

1. Login to BBG interface via remote Teamviewer. Login credentials are made available on physical BBG terminal (or any internal staff).
2. Launch UiPath Studio and open file 'BBG_VOTING_RIGHTS.xaml' found in bbg_extraction folder.
3. Create new folder 'C:\Users\Bloomberg.NICE\Dropbox\ESG Research\Quarterly Rebalancing\YYYYMM', where YYYYMM represents the year and month of quarterly rebalancing.
4. Create a copy of SBSU_BBG.xlsx from prior quarterly rebalancing folder and store it in the new folder created in Step 3.
5. Open SBSU_BBG.xlsx and replace 'securities' spreadsheet with the securities in quarterly spreadsheet. 
6. Update new filepath of SBSU_BBG.xlsx (from Step 4) in UiPath script (Under Excel Application Scope).
7. Launch these additional apps: Notepad++, Notepad
8. Press Start on UiPath Studio.
9. Once job is complete, save the Notepad++ file as VOTING_RIGHTS.txt

This job is run twice per quarter. The first run is for the securities found in the prior quarter's universe, while the second run is for the new entrants found in current quarter's universe.

Before the second run, please follow these steps:

1. Get an updated universe from OPS team.
2. Identify the new entrants and exits by comparing this updated universe with the preceding quarter's universe.
3. Update the VOTING_RIGHTS.txt by removing the exited securities.
4. Update the 'securities' spreadsheet with the new entrants.
5. Update the quarterly spreadsheet with the latest quarterly universe.
6. Rerun the extraction job for the new entrants.

### Manual Handling
1. In the event that the job was abend midway (e.g. BBG shut-down), refresh the 'securities' spreadsheet by (i) sighting the last security captured in Notepad++ and (ii) dropping the list of securities on and before this last security from the spreadsheet. Thereafter, press the Start button in UiPath Studio.
2. In the event where there are entries listed in Notepad (not Notepad++), please manually query those securities via BBG interface and update their voting rights in Notepad++ in the same format.

---

## Pre-processing

Once the extraction is complete (regardless it is the first or second run):

1. Migrate the VOTING_RIGHTS.txt from BBG terminal to local machine.
2. Run bbg_standardisation.py twice - For the first run, ensure validate = True. For the second run, set validate = False. 
3. Run eye-ball checks against BBG company names and the original company names (in the Excel output file) to ensure that there are no discrepancies. In the event of discrepancies (i.e. different company names), make a list of them. We need to manually query those securities via BBG interface and update VOTING_RIGHTS.txt accordingly.
4. Once everything is in order, rerun standardisation.py again and set validate = False.
5. Run through the Excel output file and check for securities with non-voting rights.

---
